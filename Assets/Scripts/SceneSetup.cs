﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SceneSetup : MonoBehaviour
{
    public GameObject localPlayer;

    public GameObject healthBarUI;

    public Button toggleAutoAttackButton;
    public Button[] skillButtons;
    public Button[] itemButtons;

    public Text debugWindow;

    // Start is called before the first frame update
    void Start()
    {

        localPlayer.AddComponent<UserController>();

        localPlayer.GetComponent<UserController>().CreateLocks();

        localPlayer.transform.Find("Hp").gameObject.SetActive(false);
        localPlayer.GetComponent<Unit>().healthTracker = healthBarUI;
        SkillConfigurer.Mage(localPlayer, skillButtons);
        SetCamera();
        SetUI();

    }

    
    void SetCamera ()
    {
        Vector3 pos = Camera.main.transform.position;
        pos.x = 0;
        Camera.main.transform.parent = localPlayer.transform;
        Camera.main.transform.localPosition = pos;

    }

    void SetUI ()
    {
        toggleAutoAttackButton.onClick
            .AddListener(() => localPlayer.GetComponent<UserController>().enableAutoAttack
            = !localPlayer.GetComponent<UserController>().enableAutoAttack);
    }

    void Update()
    {
    }
}
