﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FLyingTextInst : MonoBehaviour
{
    public static float speed = 70;
    public Vector3 vector;
    // Update is called once per frame
    private void OnEnable()
    {
        Invoke("Disable", .75f);
    }
    void Update()
    {
        transform.position += vector * speed * Time.deltaTime;
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }
}
