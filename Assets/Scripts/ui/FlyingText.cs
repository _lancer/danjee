﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyingText : MonoBehaviour
{

    public ObjectPool pool;
    public Transform container;
    public static FlyingText inst;
    // Start is called before the first frame update
    void Start()
    {
        inst = this;
    }

    public void Add (Vector3 at, Damage dmg)
    {
        GameObject go = pool.Get(at, false);
        go.transform.parent = container;
        go.transform.position = Camera.main.WorldToScreenPoint(at);
        go.transform.localScale = pool.prototype.transform.localScale;
        go.transform.localRotation = Quaternion.identity;
        go.transform.localPosition = go.transform.localPosition + Vector3.up * 70;
        go.GetComponent<Text>().text = dmg.Calculate() + "";
        go.GetComponent<FLyingTextInst>().vector =
           (Camera.main.WorldToScreenPoint(at)
           - Camera.main.WorldToScreenPoint(dmg.dealer.transform.position)
           ).normalized;
        go.SetActive(true);
    }
}
