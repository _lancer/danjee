﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Lobby : MonoBehaviour
{
    public InputField text;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TraceRoute());
    }

    public IEnumerator TraceRoute()
    {
        yield return null;
        try
        {
            text.text = (P2PNode.DoPing());
        }
        catch (Exception e)
        {
            text.text = e.Message;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
