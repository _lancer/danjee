﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillConfigurer 
{
    public static void Warrior(GameObject unit, Button[] skillButtons)
    {
        AutoAttack(unit, 3, .5f);
        ChangeStanceAbility(unit, skillButtons[0], unit.GetComponentInChildren<CombatSys>());
        RoundStrike(unit, skillButtons[1], unit.GetComponentInChildren<CombatSys>());
    }
    public static void Archer(GameObject unit, Button[] skillButtons)
    {
        AutoAttack(unit, 8, .75f);
        SniperShot(unit, skillButtons[0], unit.GetComponentInChildren<CombatSys>());
        Ricoshet(unit, skillButtons[1], unit.GetComponentInChildren<CombatSys>());
    }
    public static void Mage(GameObject unit, Button[] skillButtons)
    {
        AutoAttack(unit, 6, 1);
        Fireball(unit, skillButtons[0], unit.GetComponentInChildren<CombatSys>());
        Frost(unit, skillButtons[1], unit.GetComponentInChildren<CombatSys>());
    }
    public static void AutoAttack (GameObject unit, float scale, float attackspeed)
    {
        GameObject skill = new GameObject();
        skill.transform.parent = unit.transform;
        skill.AddComponent<SphereCollider>();
        skill.GetComponent<SphereCollider>().radius = 1;
        skill.GetComponent<SphereCollider>().isTrigger = true;
        skill.AddComponent<CombatSys>();
        skill.GetComponent<CombatSys>().unit = unit.GetComponent<Unit>();
        skill.GetComponent<CombatSys>().rate = attackspeed;
        skill.transform.localPosition = Vector3.zero;
        skill.transform.localScale = Vector3.one * scale;

        unit.GetComponent<UserController>().autoAttack = skill.GetComponent<CombatSys>();
    }

    public static void ChangeStanceAbility (GameObject unit, Button button, CombatSys autoAttack)
    {
        Parry p = new Parry();
        p.unit = unit.GetComponent<Unit>();
        Balance b = new Balance();
        unit.GetComponent<Unit>().IncomingDamageMods.Add(p);
        unit.GetComponent<Unit>().OutcominggDamageMods.Add(b);

        button.onClick.AddListener(() => { 
            b.active = !b.active; 
            p.active = !p.active;
            autoAttack.lastStrike = Time.time;
        });
    }

    public static void RoundStrike (GameObject unit, Button button, CombatSys autoAttack)
    {
        RoundStrike r = new RoundStrike(autoAttack, unit.GetComponent<Unit>());
        button.onClick.AddListener(() => {
            r.apply();
        });
    }

    public static void SniperShot (GameObject unit, Button button, CombatSys autoAttack)
    {
        SniperShot r = new SniperShot(autoAttack, unit.GetComponent<Unit>());
        button.onClick.AddListener(() => {
            r.apply();
        });
    }

    public static void Ricoshet (GameObject unit, Button button, CombatSys autoAttack)
    {
        Ricoshet r = new Ricoshet(autoAttack, unit.GetComponent<Unit>());
        button.onClick.AddListener(() => {
            r.apply();
        });
    }

    public static void Fireball(GameObject unit, Button button, CombatSys autoAttack)
    {
        Fireball r = new Fireball(autoAttack, unit.GetComponent<Unit>());
        button.onClick.AddListener(() => {
            r.apply();
        });
    }
    public static void Frost(GameObject unit, Button button, CombatSys autoAttack)
    {
        Frost r = new Frost(autoAttack, unit.GetComponent<Unit>());
        button.onClick.AddListener(() => {
            r.apply();
        });
    }
}
