﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* implement a partial freeze on bosses */
public class Frost
{
    CombatSys weaponRange;
    float lastUsed = 0;
    public float rate = 3.5f;
    public float duration = 2f;
    Unit unit; 

    public Frost(CombatSys weaponRange, Unit unit)
    {
        this.weaponRange = weaponRange;
        this.unit = unit;
    }

    public void apply()
    {
        if (Time.time - lastUsed < rate) return;

        Unit x = weaponRange.inRange[0];
        if (x == null) return;

        Damage.Deal(x, unit, unit.GetDamage());

        x.ApplyStun(duration);
        weaponRange.lastStrike = Time.time; 
        lastUsed = Time.time;
    }


}
