﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    CombatSys weaponRange;
    float lastUsed = -5f;
    public float rate = 5f;
    Unit unit;
    GameObject range;
     
    public Fireball(CombatSys weaponRange, Unit unit)
    {
        this.weaponRange = weaponRange;
        this.unit = unit;

        range = new GameObject();

        range.AddComponent<SphereCollider>();
        range.GetComponent<SphereCollider>().radius = 2;
        range.GetComponent<SphereCollider>().isTrigger = true;
        range.AddComponent<Range>(); 
        range.GetComponent<Range>().root = this; 
        range.SetActive(false);
    }

    public void apply()
    {
        if (Time.time - lastUsed < rate) return;

        Unit x = weaponRange.inRange[0];
        if (x == null) return;
        range.SetActive(false);

        weaponRange.lastStrike = Time.time;
       
        range.transform.position = x.transform.position;
        range.SetActive(true);

        lastUsed = Time.time;
    }
    public void Strike(Unit unit)
    {
        Damage d = this.unit.GetDamage();
        d.coeff = 3;

        Damage.Deal(unit, this.unit, d);

        d = this.unit.GetDamage();
        d.coeff = .25f;
        d.tiks = 4;
        unit.periodic.Add(d);
    }

    public class Range : MonoBehaviour
    {
        public Fireball root;

        public void OnEnable()
        {
            Invoke("Disable", .2f);
        }
        public void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Unit>() != null)
                root.Strike(other.GetComponent<Unit>());
        }
        void Disable ()
        {
            gameObject.SetActive(false);
        }
       
    }
}
