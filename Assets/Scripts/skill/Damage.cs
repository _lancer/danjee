﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage
{
    public interface Modifier
    {
        void apply(Damage damage);
    }

    public float coeff = 1;
    public float amount = 40;
    public Unit dealer;

    public int tiks;
    public Damage (float coeff, float amount, Unit dealer)
    {
        this.coeff = coeff;
        this.amount = amount;
        this.dealer = dealer;
    }

    public float Calculate ()
    {
        return coeff * amount;
    }
    public Damage clone ()
    {
        return new Damage(coeff, amount, dealer);
    }

    public static void Deal (Unit target, Unit dealer, Damage damage)
    {
        target.RecieveDamage(damage);
        FlyingText.inst.Add(target.transform.position + Vector3.one, damage);
    }
}
