﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperShot
{
    CombatSys weaponRange;
    float lastUsed = 0;
    public float rate = 2.5f;
    Unit unit;
    public SniperShot(CombatSys weaponRange, Unit unit)
    {
        this.weaponRange = weaponRange;
        this.unit = unit;
    }

    public void apply()
    {
        if (Time.time - lastUsed < rate) return;

        Unit x = weaponRange.inRange[0];
        if (x == null) return;

        weaponRange.lastStrike = Time.time;

        Damage d = unit.GetDamage();
        d.coeff = 3;
        Damage.Deal(x, unit, d);
        lastUsed = Time.time;

    }
}
