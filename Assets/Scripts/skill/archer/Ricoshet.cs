﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ricoshet 
{
    CombatSys weaponRange;
    float lastUsed = 0;
    public float rate = 2.5f;
    Unit unit;
    GameObject range;
    public int jumps = 4;

    public List<Unit> damaged = new List<Unit>();
    public Ricoshet(CombatSys weaponRange, Unit unit)
    {
        this.weaponRange = weaponRange;
        this.unit = unit;

        range = new GameObject();

        range.AddComponent<SphereCollider>();
        range.GetComponent<SphereCollider>().radius = 6;
        range.GetComponent<SphereCollider>().isTrigger = true;
        range.AddComponent<Range>();
        range.GetComponent<Range>().root = this;
        range.SetActive ( false);
    }

    public void apply ()
    {
        if (Time.time - lastUsed < rate) return;

        Unit x = weaponRange.inRange[0];
        if (x == null) return;
        range.SetActive(false);

        damaged.Clear();
        jumps = 3;

        weaponRange.lastStrike = Time.time;
        damaged.Add(x);

        Damage.Deal(x, unit, unit.GetDamage());

        range.transform.position = x.transform.position;
        range.SetActive(true);

        lastUsed = Time.time;
    }

    public void jumpTo (Unit unit)
    {

        jumps--;
        damaged.Add(unit);

        Damage.Deal(unit, this.unit, this.unit.GetDamage());

        if (jumps > 0)
        {
            range.transform.position = unit.transform.position;

        } else
            range.SetActive(false);

    }

    public class Range : MonoBehaviour
    {
        public Ricoshet root;
        public void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Unit>() != null)
                if (!root.damaged.Contains (other.GetComponent<Unit>()))
                    Strike(other.GetComponent<Unit>());
        }
        public void Strike(Unit target)
        {
            print("jumoing");
            root.jumpTo(target);
        }
    }
}
