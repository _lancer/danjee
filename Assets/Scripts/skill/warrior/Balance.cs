﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balance : Damage.Modifier
{

    public bool active = true;
    public float coeff = .25f;
    public void apply(Damage damage)
    {
        if (active)
            damage.coeff += .25f;
    }
}
