﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundStrike
{
    CombatSys weaponRange;
    float lastUsed = 0;
    public float rate = 2.5f;
    Unit unit;
    public RoundStrike (CombatSys weaponRange, Unit unit)
    {
        this.weaponRange = weaponRange;
        this.unit = unit;
    }

    public void apply()
    {
        if (Time.time - lastUsed < rate) return;

        weaponRange.lastStrike = Time.time;
        weaponRange.inRange.ForEach(x =>
        {
            Damage.Deal(x, unit, unit.GetDamage());
            lastUsed = Time.time;
        });

    }
}
