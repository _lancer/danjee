﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parry : Damage.Modifier
{
    float lastUsed = 0;
    public float rate = 0.5f;
    public float amount = 40;
    public bool active = false;
    public Unit unit;
    public void apply (Damage damage)
    {
        if (!active) return;
        if (Time.time - lastUsed < rate) return;

        damage.amount -= unit.GetDamage().Calculate();

        Debug.Log("parry");

    }
}
