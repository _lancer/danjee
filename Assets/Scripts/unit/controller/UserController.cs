﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserController : MonoBehaviour
{
    public CombatSys autoAttack;
    public bool enableAutoAttack = true;

    bool blockedL;
    bool blockedR;
    bool blockedF;
    bool blockedB;

    public void CreateLocks ()
    {
        CreateBlock(BlockL, -Vector3.right);
        CreateBlock(BlockR, Vector3.right);
        CreateBlock(BlockF, Vector3.forward);
        CreateBlock(BlockB, -Vector3.forward);
    }

    public void CreateBlock (Action<bool> action, Vector3 mod)
    {
        GameObject go = new GameObject();
        go.AddComponent<SphereCollider>();
        go.GetComponent<SphereCollider>().isTrigger = true;
        
        go.layer = LayerMask.NameToLayer("Border");
        go.AddComponent<Block>();

        go.GetComponent<Block>().block = action;

        go.transform.parent = transform;
        go.transform.localPosition += mod;
    }
    public void BlockL (bool val)
    {
        blockedL = val;
    }
    public void BlockR(bool val)
    {
        blockedR = val;
    }
    public void BlockF(bool val)
    {
        blockedF = val;
    }
    public void BlockB(bool val)
    {
        blockedB = val;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float x = JoystickLeft.positionX;
        if (blockedL && x < 0)
            x = 0;
        if (blockedR && x > 0)
            x = 0;
        float y = JoystickLeft.positionY;
        if (blockedF && y > 0)
            y = 0;
        if (blockedB && y < 0)
            y = 0;
        transform.position = new Vector3(transform.position.x +
            x * 20 * Time.deltaTime, transform.position.y, transform.position.z +
            y * 20 * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (enableAutoAttack)
            if (JoystickLeft.positionX == 0 && JoystickLeft.positionY == 0)
                if (autoAttack.CanStrike()) autoAttack.Strike();
    }
}
