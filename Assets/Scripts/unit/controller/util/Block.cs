﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public Action<bool> block;
    
    List<Collider> colliders = new List<Collider>();
    
    private void OnTriggerEnter(Collider other)
    {
        colliders.Add(other);
        if (colliders.Count > 0)
            block(true);
    }
    private void OnTriggerExit(Collider other)
    {
        colliders.Remove(other);
        if (colliders.Count == 0)
            block(false);
    }
}
