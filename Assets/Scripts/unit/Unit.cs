﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public float maxHealth;
    float currentHealth;

    public GameObject healthTracker;

    public Damage basicDamage;
    public List<Damage.Modifier> IncomingDamageMods = new List<Damage.Modifier>();
    public List<Damage.Modifier> OutcominggDamageMods = new List<Damage.Modifier>();

    public List<Damage> periodic = new List<Damage>();

    Stun stun = new Stun();

    // Start is called before the first frame update
    void Start()
    {
        basicDamage = new Damage(1, 40, this);
        currentHealth = maxHealth;

        InvokeRepeating("doPeriodic", 1, 1);
    }

    private void doPeriodic()
    {
        foreach (Damage d in periodic)
        {

            Damage.Deal(this, d.dealer, d);

            d.tiks -= 1;

        }
        periodic.RemoveAll(x => x.tiks <=0);

    }
    public void RecieveDamage (Damage damage)
    {
        foreach (Damage.Modifier mod in IncomingDamageMods)
            mod.apply(damage);

        currentHealth -= damage.Calculate();

        if (currentHealth < 0)
            currentHealth = 0;
        if (currentHealth > maxHealth)
            currentHealth = maxHealth;

        Vector3 v = Vector3.one;
        v.x =  currentHealth /  maxHealth;

        healthTracker.transform.localScale = v;

        if (currentHealth <= 0)
            Die();
    }

    public Damage GetDamage ()
    {
        Damage clone = basicDamage.clone();
        foreach (Damage.Modifier mod in OutcominggDamageMods)
            mod.apply(clone);
        return clone;
    }

    class Stun
    {
        public float applied;
        public float duration;
    }
    public void ApplyStun(float duration) 
    {
        float newExpectedEnd = Time.time + duration;
        if ( newExpectedEnd > stun.applied+stun.duration)
        {
            stun.applied = Time.time;
            stun.duration = duration;
            CancelInvoke("EnableControls");
            DisableControls();
            Invoke("EnableControls", duration);
        }
        
    }
    void DisableControls ()
    {
        if (GetComponent<UserController>() != null)
            GetComponent<UserController>().enabled = false;

        if (GetComponentInChildren<CombatSys>() != null)
            GetComponentInChildren<CombatSys>().enabled = false;

        print("disabled");
    }

    void EnableControls()
    {
        if (GetComponent<UserController>() != null)
            GetComponent<UserController>().enabled = false;
        if (GetComponentInChildren<CombatSys>() != null)
            GetComponentInChildren<CombatSys>().enabled = false;

        print("enabled");

    }

    void Die ()
    {
        print("I'm dead");
    }
}
