﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatSys : MonoBehaviour
{
    public Unit unit;
    public float rate = .5f;
    public List<Unit> inRange = new List<Unit>();
    public float lastStrike = 0;
    // Start is called before the first frame update
    public bool CanStrike ()
    {
        return inRange.Count > 0 && Time.time - lastStrike >= rate;
    } 

    public void Strike ()
    {
        // print("attack " + inRange[0].name + " " + unit.GetDamage().amount);
        Damage.Deal(inRange[0], unit, unit.GetDamage());
        lastStrike = Time.time;
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Unit>() != null)
            inRange.Add(other.GetComponent<Unit>());
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Unit>() != null)
            inRange.Remove(other.GetComponent<Unit>());
    }
}
