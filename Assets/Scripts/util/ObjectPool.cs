﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{

    public GameObject prototype;
    List<GameObject> pool = new List<GameObject>();
    
    public GameObject Get (Vector3 at, bool activate = true)
    {
        foreach (GameObject obj in pool)
        {
            if (!obj.activeSelf)
            {
                obj.transform.position = at;
                obj.SetActive(activate);
                return obj;
            }
        }

        GameObject obj_ = Instantiate(prototype, at, Quaternion.identity);
        pool.Add(obj_);
        obj_.SetActive(activate);
        return obj_;
    }
}
